package com.jb.producto.mapper;

import com.jb.producto.dto.ProductoDto;
import com.jb.producto.entitis.Producto;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-11-16T14:27:00-0500",
    comments = "version: 1.5.1.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-8.4.jar, environment: Java 17.0.8 (Oracle Corporation)"
)
@Component
public class IProductoMapperImpl implements IProductoMapper {

    @Override
    public ProductoDto toProductoDto(Producto producto) {
        if ( producto == null ) {
            return null;
        }

        ProductoDto.ProductoDtoBuilder productoDto = ProductoDto.builder();

        productoDto.codigo( producto.getCodigo() );
        productoDto.nombre( producto.getNombre() );
        productoDto.descripcion( producto.getDescripcion() );
        productoDto.precio( producto.getPrecio() );

        return productoDto.build();
    }

    @Override
    public Producto toProducto(ProductoDto productoDto) {
        if ( productoDto == null ) {
            return null;
        }

        Producto producto = new Producto();

        producto.setCodigo( productoDto.getCodigo() );
        producto.setNombre( productoDto.getNombre() );
        producto.setDescripcion( productoDto.getDescripcion() );
        producto.setPrecio( productoDto.getPrecio() );

        return producto;
    }

    @Override
    public List<ProductoDto> listProductoDto(List<Producto> sourceList) {
        if ( sourceList == null ) {
            return null;
        }

        List<ProductoDto> list = new ArrayList<ProductoDto>( sourceList.size() );
        for ( Producto producto : sourceList ) {
            list.add( toProductoDto( producto ) );
        }

        return list;
    }
}
