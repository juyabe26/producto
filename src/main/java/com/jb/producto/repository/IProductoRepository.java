package com.jb.producto.repository;

import com.jb.producto.entitis.Producto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IProductoRepository extends JpaRepository<Producto, Long> {
    @Query("SELECT p FROM Producto p WHERE p.codigo = :codigo")
    Producto buscarPorCodigo(@Param("codigo") String codigo);
}
