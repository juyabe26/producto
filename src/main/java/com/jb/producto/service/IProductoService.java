package com.jb.producto.service;

import com.jb.producto.dto.ProductoDto;

import java.util.List;

public interface IProductoService {
    ProductoDto crearProducto(ProductoDto productoDto);

    ProductoDto buscarProductoPorCodigo(String codigo);

    List<ProductoDto> obtenerTodosLosProductos();

    ProductoDto actualizarProducto(ProductoDto productoDto);

    void eliminarProducto(String codigo);
}
