package com.jb.producto.service.impl;
import com.jb.producto.dto.ProductoDto;
import com.jb.producto.entitis.Producto;
import com.jb.producto.excepcion.BusinessException;
import com.jb.producto.mapper.IProductoMapper;
import com.jb.producto.repository.IProductoRepository;
import com.jb.producto.service.IProductoService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
@AllArgsConstructor
public class ProductoService implements IProductoService {

    private final IProductoRepository productoRepository;
    private final IProductoMapper productoMapper;

    @Override
    public ProductoDto crearProducto(ProductoDto productoDto) {
        if (productoRepository.buscarPorCodigo(productoDto.getCodigo()) != null)
            throw new BusinessException("P-404", HttpStatus.INTERNAL_SERVER_ERROR, "El codigo ya existe");
        Producto producto = productoMapper.toProducto(productoDto);
        return productoMapper.toProductoDto(productoRepository.save(producto));
    }

    @Override
    public ProductoDto buscarProductoPorCodigo(String codigo) {
        return productoMapper.toProductoDto(buscarProducto(codigo));
    }

    @Override
    public List<ProductoDto> obtenerTodosLosProductos() {
        return productoMapper.listProductoDto(productoRepository.findAll());
    }

    @Override
    public ProductoDto actualizarProducto(ProductoDto productoDto) {
        Producto producto = buscarProducto(productoDto.getCodigo());
        producto.setNombre(productoDto.getNombre());
        producto.setDescripcion(productoDto.getDescripcion());
        producto.setPrecio(producto.getPrecio());
        return productoMapper.toProductoDto(productoRepository.save(producto));
    }

    @Override
    public void eliminarProducto(String codigo) {
        productoRepository.delete(buscarProducto(codigo));
    }


    private Producto buscarProducto(String codigo){
        Producto producto = productoRepository.buscarPorCodigo(codigo);
        if (Objects.isNull(producto))
            throw new BusinessException("P-404", HttpStatus.INTERNAL_SERVER_ERROR, "Datos no encontrados");
        return producto;
    }

}
