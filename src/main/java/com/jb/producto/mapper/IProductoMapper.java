package com.jb.producto.mapper;

import com.jb.producto.dto.ProductoDto;
import com.jb.producto.entitis.Producto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface IProductoMapper {

    ProductoDto toProductoDto(Producto producto);
    Producto toProducto(ProductoDto productoDto);

    List<ProductoDto> listProductoDto(List<Producto> sourceList);

}
