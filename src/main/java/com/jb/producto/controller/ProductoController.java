package com.jb.producto.controller;

import com.jb.producto.dto.ProductoDto;
import com.jb.producto.entitis.Producto;
import com.jb.producto.service.IProductoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/producto")
@AllArgsConstructor
public class ProductoController {

    private final IProductoService productoService;


    @PostMapping
    @Operation(summary = "Crear Producto", description = "Crear un producto con los parametros indicados.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Producto Creado con Exito"),
            @ApiResponse(responseCode = "404", description = "Producto ya exite")})
    public ProductoDto crearAgenda(@RequestBody ProductoDto productoDto) {
        return productoService.crearProducto(productoDto);
    }

   @GetMapping("/todos")
   @Operation(summary = "Buscar todos los Producto", description = "Buscar todos los  productos existentes.")
   @ApiResponses(value = {
           @ApiResponse(responseCode = "200", description = "Productos encontrados")})
    public List<ProductoDto> obtenerTodosLosProductos() {
        return productoService.obtenerTodosLosProductos();
    }

    @GetMapping
    @Operation(summary = "Buscar Producto", description = "Buscar un producto especifico a partir del codigo.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Producto encontrado con Exito"),
            @ApiResponse(responseCode = "404", description = "Producto no existe")})
    public ProductoDto buscarProductoPorCodigo(@RequestParam(value = "codigo") String codigo) {
        return productoService.buscarProductoPorCodigo(codigo);
    }

    @PutMapping
    @Operation(summary = "Actualizar Producto", description = "Actulizar un producto especifico a partir del codigo.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Producto actualizado con Exito"),
            @ApiResponse(responseCode = "404", description = "Producto no existe")})
    public ProductoDto actualizarProducto(@RequestBody ProductoDto productoDto) {
        return productoService.actualizarProducto(productoDto);
    }

    @DeleteMapping
    @Operation(summary = "eliminar Producto", description = "Eliminar un producto especifico a partir del codigo.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Producto Eliminado con Exito"),
            @ApiResponse(responseCode = "404", description = "Producto no existe")})
    public void eliminarProducto(@RequestParam(value = "codigo") String codigo) {
        productoService.eliminarProducto(codigo);
    }

}
