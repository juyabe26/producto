package com.jb.producto.excepcion;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestException extends RuntimeException{
    private String code;
    public RequestException(String code, String message) {
        super(message);
        this.code = code;
    }
}
