package com.jb.producto.excepcion;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BusinessException extends RuntimeException{

    private String code;
    private HttpStatus status;

    public BusinessException(String code, HttpStatus status, String message){
        super(message);
        this.code = code;
        this.status = status;
    }
}
