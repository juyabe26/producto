<h1 align="center"> Imagen Docker</h1>

- `Paso 1`: Generar el Jar del aplicativo
  - ./gradlew clean build
- `Paso 2`: Crear la imagen 
  - docker build -t producto:tag .
- `Paso 3`: Subir el contenedor por el puerto definido el el Dokerfile
  - docker run -p 8080:8080 producto:tag

<h1 align="center"> Ruta Swagger</h1>

- `Ruta`: http://localhost:8080/swagger-ui.html